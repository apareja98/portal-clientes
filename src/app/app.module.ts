import { AuthGuardService } from './guards/auth-guard.service';
import { TipoDocumentoService } from './services/tipo-documento.service';
import { TransaccionService } from './services/transaccion.service';
import { CuentaService } from './services/cuenta.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ClientesService } from './services/clientes.service';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { CredencialesService } from './services/credenciales.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ListaCuentasComponent } from './components/lista-cuentas/lista-cuentas.component';
import { UltimasTransaccionesComponent } from './components/ultimas-transacciones/ultimas-transacciones.component';
import { TransaccionesCuentaComponent } from './components/transacciones-cuenta/transacciones-cuenta.component';
import { CuentasInscritasComponent } from './components/cuentas-inscritas/cuentas-inscritas.component';
import { InscribirCuentaComponent } from './components/inscribir-cuenta/inscribir-cuenta.component';
import { InscribirCuentaAjenaComponent } from './components/inscribir-cuenta-ajena/inscribir-cuenta-ajena.component';
import { ListarCuentasRegistradasComponent } from './listar-cuentas-registradas/listar-cuentas-registradas.component';
import { TrasladoComponent } from './components/traslado/traslado.component';
import { ListaTransaccionesComponent } from './components/lista-transacciones/lista-transacciones.component';
import { EditarPerfilComponent } from './components/editar-perfil/editar-perfil.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    ListaCuentasComponent,
    UltimasTransaccionesComponent,
    TransaccionesCuentaComponent,
    CuentasInscritasComponent,
    InscribirCuentaComponent,
    InscribirCuentaAjenaComponent,
    ListarCuentasRegistradasComponent,
    TrasladoComponent,
    ListaTransaccionesComponent,
    EditarPerfilComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
    
  ],
  providers: [AuthGuardService,ClientesService, CredencialesService, CuentaService,TransaccionService,TipoDocumentoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
