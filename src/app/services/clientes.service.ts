import { GLOBAL } from './../GLOBAL';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Respuesta } from '../domain/respuesta';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {
  url: String = GLOBAL.url + 'cliente';
  constructor(private http: HttpClient) { }


public login(cuenid:String, clieId: Number, clave:String):Observable<any>{
  let body={"cuenId":cuenid,"clieId":clieId,"clave":clave}
  return this.http.post(this.url+ '/login', body);
}
public validarCuenta(clieId:Number,tiDoc:Number,cuenId:String):Observable<any>{
  return this.http.get(this.url+'/findClienteDatos/'+clieId+'/'+tiDoc+'/'+cuenId);
}

public update (cliente):Observable<Respuesta>{
  return this.http.put<Respuesta>(this.url+'/update',cliente);
}

}