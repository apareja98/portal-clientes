import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GLOBAL } from './../GLOBAL';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TipoDocumentoService {
  url: String = GLOBAL.url;
  constructor(private http:HttpClient) { }

  getAllTipoDocumento():Observable<any[]>{
    return this.http.get<any[]>(this.url+'tipoDocumento/findAll');
  }
}
