import { Respuesta } from './../domain/respuesta';
import { GLOBAL } from './../GLOBAL';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CredencialesService } from './credenciales.service';
import { Observable } from 'rxjs';
import { Cuenta } from '../domain/cuenta';


@Injectable({
  providedIn: 'root'
})
export class CuentaService {
  url : string = GLOBAL.url;
  constructor(private http: HttpClient, private cs : CredencialesService) { }
  public getAllCuentas():Observable<Cuenta[]>{
    return this.http.get<Cuenta[]>(this.url+ 'cuenta/cuentasCliente/'+ this.cs.getCliente().clieId);
  }
  public getAllCuentasRegistradas():Observable<Cuenta[]>{
    return this.http.get<Cuenta[]>(this.url+'cuenta/cuentasRegistradasCliente/'+ this.cs.getCliente().clieId);
  }
  public findById(id:String):Observable<Cuenta>{
    return this.http.get<Cuenta>(this.url+'cuenta/findById/'+id);
  }
  public cambiarClave(cuenId:String, antigua:String, nueva:String):Observable<Respuesta>{
    let body = {cuenId:cuenId, claveAntigua: antigua,claveNueva:nueva};
    return this.http.post<Respuesta>(this.url+'cuenta/cambioClave',body);

  }
  public registrarCuenta(clieId:Number, tdocId:Number, id:Number,cuenId:String):Observable<Respuesta>{
    let body={clieId:clieId,tiDocId:tdocId,id:id,cuenId:cuenId};
    return this.http.post<Respuesta>(this.url+'cuenta/registrarCuenta',body);
  }
}
