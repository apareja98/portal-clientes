import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CredencialesService {
  private cliente :any;
  private isLogged :boolean;
  constructor() { }

  setCliente(cliente:any){
    this.cliente = cliente;
  }
  getCliente(){
    return this.cliente;
  }
  setIsLogged(isLogged:boolean){
    this.isLogged=isLogged;
  }
  getIsLogged(){
    return this.isLogged;
  }

}
