import { HttpClient } from '@angular/common/http';
import { CredencialesService } from './credenciales.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Transaccion } from '../domain/transaccion';

import { GLOBAL } from '../GLOBAL';
import { Respuesta } from '../domain/respuesta';

@Injectable({
  providedIn: 'root'
})
export class TransaccionService {
  url: String = GLOBAL.url+'transaccionBancaria';
  private cuentaSeleccionada:String = null;
  constructor(private http: HttpClient, private cs : CredencialesService) { }

  public getLatestTransacciones(): Observable<Transaccion[]>{
    return this.http.get<Transaccion[]>(this.url+'/ultimas/'+this.cs.getCliente().clieId);
    
  }
  public getTransaccionesPorCuenta(cuenId:String):Observable<Transaccion[]>{
    return this.http.get<Transaccion[]>(this.url+'/transaccionesPorCuenta/'+ cuenId);
  }
  public getCuentaSeleccionada(){
    return this.cuentaSeleccionada;
  }
  public setCuentaSeleccionada(cuenId:String){
    this.cuentaSeleccionada = cuenId;
  }
  public limpiarCuenta(){
    this.cuentaSeleccionada=null;
  }
  public traslado(cuenIdOrigen:String,cuenIdDestino:String, usuario:String, valor:Number):Observable<Respuesta>{
    let body={cuenIdOrigen:cuenIdOrigen,cuenIdDestino:cuenIdDestino,usuUsuario:usuario,valor:valor};
    return this.http.post<Respuesta>(this.url+'/traslado',body);
  }
}

