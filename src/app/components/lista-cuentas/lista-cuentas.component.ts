import { Router } from '@angular/router';
import { CuentaService } from './../../services/cuenta.service';
import { Component, OnInit } from '@angular/core';
import { Cuenta } from '../../domain/cuenta';
import { TransaccionService } from '../../services/transaccion.service';

@Component({
  selector: 'app-lista-cuentas',
  templateUrl: './lista-cuentas.component.html',
  styleUrls: ['./lista-cuentas.component.css']
})
export class ListaCuentasComponent implements OnInit {
  cuentas : Cuenta[];
  constructor(private cs:CuentaService, private ts:TransaccionService, private router:Router) { }

  ngOnInit() {
    this.getCuentas()
  }
  getCuentas(){
    this.cs.getAllCuentas().subscribe(res=>{
      this.cuentas=res;
    })
  }
  verTransacciones(cuenId:String){
    this.ts.setCuentaSeleccionada(cuenId);
    console.log(cuenId)
    this.router.navigate(['detalleCuenta']);

  }
}
