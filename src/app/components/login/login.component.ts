import { Router } from '@angular/router';
import { ClientesService } from './../../services/clientes.service';
import { Component, OnInit } from '@angular/core';
import { error } from '@angular/compiler/src/util';
import { CredencialesService } from '../../services/credenciales.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  clieId : Number = null;
  cuenId : String ="";
  clave : String ="";
  mensaje ="";
  mostrarAlerta=false;
  constructor(private cs: ClientesService, private credencialesService : CredencialesService, private router :Router) { }

  ngOnInit() {
  }
  login(){
    console.log("cliente: " , this.clieId);
    console.log("cuenId: " , this.cuenId);
    console.log("clave: " , this.clave);
    this.cs.login(this.cuenId,this.clieId,this.clave).subscribe(res=>{
      console.log(res);
      this.credencialesService.setCliente(res.cliente);
      this.credencialesService.setIsLogged(true);
      this.router.navigate(['/home']);
    },error=>{
      this.mostrarAlerta=true;
      console.log(error.error);
      this.mensaje=error.error.mensaje;
    })
  }
}
