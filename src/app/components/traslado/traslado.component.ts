import { CredencialesService } from './../../services/credenciales.service';
import { TransaccionService } from './../../services/transaccion.service';
import { CuentaService } from './../../services/cuenta.service';
import { Cuenta } from './../../domain/cuenta';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-traslado',
  templateUrl: './traslado.component.html',
  styleUrls: ['./traslado.component.css']
})
export class TrasladoComponent implements OnInit {

  cuentas : Cuenta[];
  cuentasRegistradas:Cuenta[];
  cuentaOrigen : Cuenta;
  cuentaDestino: Cuenta;
  valor : Number;
  mensaje:String="";
  mostrarAlerta:Boolean=false;
  mostrarExito:Boolean=false;
  constructor(private cs:CuentaService, private ts:TransaccionService,private credenciales:CredencialesService) { }

  ngOnInit() {
    this.getCuentas();
    this.getCuentasRegistradas();
  }
  getCuentasRegistradas(){
    this.cs.getAllCuentasRegistradas().subscribe(res=>{
      this.cuentasRegistradas= res;
    })
  }

  getCuentas(){
    this.cs.getAllCuentas().subscribe(res=>{
      this.cuentas=res;
    })
  }
  seleccionarCuentaOrigen(e:any){
    this.cuentaOrigen=e;

  }
 
  seleccionarCuentaDestino(e:any){
    this.cuentaDestino=e;

  }
  realizarTraslado(){
    if(this.cuentaOrigen.cuenId!=this.cuentaDestino.cuenId){
      if(this.valor){
        this.ts.traslado(this.cuentaOrigen.cuenId,this.cuentaDestino.cuenId,'banco_web',this.valor).subscribe(res=>{
          this.mensaje=res.mensaje;
          this.mostrarExito=true;
          this.mostrarAlerta=false;
        },error=>{
          this.mensaje=error.error.mensaje;
          this.mostrarAlerta=true;
          this.mostrarExito=false;
        });
  
      }else{
        this.mostrarAlerta=true;
      this.mensaje="Ingresa un valor";
     
      }
     
    }else{
      this.mostrarAlerta=true;
      this.mensaje="Selecciona cuentas diferentes";
    }
    
  }
}
