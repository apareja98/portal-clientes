import { Transaccion } from './../../domain/transaccion';
import { TransaccionService } from './../../services/transaccion.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cuenta } from '../../domain/cuenta';
import { CuentaService } from '../../services/cuenta.service';

@Component({
  selector: 'app-transacciones-cuenta',
  templateUrl: './transacciones-cuenta.component.html',
  styleUrls: ['./transacciones-cuenta.component.css']
})
export class TransaccionesCuentaComponent implements OnInit {

  cuenta : Cuenta;
  transacciones : Transaccion[];
  dialogCambioClave : boolean =false;
  claveAnterior:String =null;
  claveNueva:String =null;
  mensajeAprobacion=null;
  mensajeError =null;
  dialogAprobacion: boolean=false;
  dialogError: boolean=false;
  constructor(private ts: TransaccionService, private router: Router, private cs:CuentaService) { }

  ngOnInit() {
    this.getDataCuenta();
  }
  getDataCuenta(){
    let selectedCuenta = this.ts.getCuentaSeleccionada();
    if(selectedCuenta){
      this.ts.getTransaccionesPorCuenta(selectedCuenta).subscribe(res=>{
        this.transacciones = res;
      })
    }
    this.cs.findById(selectedCuenta).subscribe(res=>{
      this.cuenta= res;
    })
    this.ts.limpiarCuenta();
  }
  showDialog(){
    this.dialogCambioClave=true;
  }
  closeDialog(){
    this.dialogCambioClave=false;
    this.claveAnterior=null;
    this.claveNueva=null;
  }
  cambiarClave(){
    if(this.claveAnterior && this.claveNueva){
      this.cs.cambiarClave(this.cuenta.cuenId, this.claveAnterior, this.claveNueva).subscribe(res=>{
        this.mensajeAprobacion=res.mensaje;
        this.dialogAprobacion=true;
        this.dialogError=false;
      },error=>{
        this.mensajeError=error.error.mensaje;
        this.dialogAprobacion = false;
        this.dialogError=true;
      })
    }
  }
}
