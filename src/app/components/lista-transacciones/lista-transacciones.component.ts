import { TransaccionService } from './../../services/transaccion.service';
import { Transaccion } from './../../domain/transaccion';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-transacciones',
  templateUrl: './lista-transacciones.component.html',
  styleUrls: ['./lista-transacciones.component.css']
})
export class ListaTransaccionesComponent implements OnInit {
  transacciones :Transaccion[];
  constructor(private ts : TransaccionService) { }

  ngOnInit() {
   
  }
  

}
