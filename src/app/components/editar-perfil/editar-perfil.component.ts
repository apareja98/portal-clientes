import { CredencialesService } from './../../services/credenciales.service';
import { ClientesService } from './../../services/clientes.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.component.html',
  styleUrls: ['./editar-perfil.component.css']
})
export class EditarPerfilComponent implements OnInit {
  cliente : any;
  mensaje : String;
  mostrarAlerta:Boolean=false;
  mostrarExito: Boolean=false;
  constructor(private clienteService : ClientesService, private credenciales:CredencialesService) { }

  ngOnInit() {
    this.cliente = this.credenciales.getCliente();
  }
  actualizar(){
    this.clienteService.update(this.cliente).subscribe(res=>{
      this.mensaje=res.mensaje;
      this.mostrarExito=true;
      this.mostrarAlerta=false;
      this.credenciales.setCliente(this.cliente);
    },error=>{
      this.mostrarAlerta=true;
      this.mostrarExito=false;
      this.mensaje=error.error.mensaje
    })
  }
}
