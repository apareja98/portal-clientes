import { Component, OnInit } from '@angular/core';
import { TransaccionService } from '../../services/transaccion.service';
import { Transaccion } from '../../domain/transaccion';

@Component({
  selector: 'app-ultimas-transacciones',
  templateUrl: './ultimas-transacciones.component.html',
  styleUrls: ['./ultimas-transacciones.component.css']
})
export class UltimasTransaccionesComponent implements OnInit {
  transacciones :Transaccion[];
  constructor(private ts : TransaccionService) { }

  ngOnInit() {
    this.getUltimasTransacciones();
  }
  getUltimasTransacciones(){
    this.ts.getLatestTransacciones().subscribe(res=>{
      this.transacciones = res;
      console.log(res);
    })
  }

}
