import { Router } from '@angular/router';
import { CredencialesService } from './../../services/credenciales.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  nombreCliente :String ="";
  constructor(private cs: CredencialesService, private router:Router) { }

  ngOnInit() {
    this.nombreCliente = this.cs.getCliente().nombre;
  }
  logout(){
    this.cs.setCliente(null);
    this.cs.setIsLogged(false);
    this.router.navigate(['/login']);
  }

}
