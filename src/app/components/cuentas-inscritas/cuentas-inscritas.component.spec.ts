import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentasInscritasComponent } from './cuentas-inscritas.component';

describe('CuentasInscritasComponent', () => {
  let component: CuentasInscritasComponent;
  let fixture: ComponentFixture<CuentasInscritasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentasInscritasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuentasInscritasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
