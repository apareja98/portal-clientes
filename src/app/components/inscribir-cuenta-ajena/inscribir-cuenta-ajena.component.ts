import { CredencialesService } from './../../services/credenciales.service';
import { error } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { TipoDocumentoService } from '../../services/tipo-documento.service';
import { ClientesService } from '../../services/clientes.service';
import { CuentaService } from '../../services/cuenta.service';

@Component({
  selector: 'app-inscribir-cuenta-ajena',
  templateUrl: './inscribir-cuenta-ajena.component.html',
  styleUrls: ['./inscribir-cuenta-ajena.component.css']
})
export class InscribirCuentaAjenaComponent implements OnInit {
  tiposDocumentos=[];
 
  cuenId : String;
  id : Number;
  tiDoc : Number=-1;
  cliente : any;
  mostrarAlerta=false;
  mensaje:String ="";
  mostrarExito=false;
  constructor(private tipoDocumentoService:TipoDocumentoService, 
    private clienteService:ClientesService, private cs:CuentaService, private credenciales:CredencialesService) { }

  ngOnInit() {
    this.getTiposDocumentos();
  }
  getTiposDocumentos():void{
    this.tipoDocumentoService.getAllTipoDocumento().subscribe(res=>{
      this.tiposDocumentos=res;
      console.log(res);
    })
  }
  validar(){
    if(this.id && this.cuenId && this.tiDoc ){
      this.clienteService.validarCuenta(this.id,this.tiDoc,this.cuenId).subscribe(res=>{
        this.cliente=res;
        this.mostrarAlerta=false;
        this.mostrarExito=false;
      },error=>{
        console.log(error.error);
        this.mostrarAlerta=true;
        this.mostrarExito=false;
        this.mensaje="Revisa los datos de la cuenta"
        this.cliente=null;
      })
    }
  }
  registrar(){
    if(this.cliente!=null){
      this.cs.registrarCuenta(this.credenciales.getCliente().clieId,this.tiDoc,this.id,this.cuenId).subscribe(res=>{
        this.mensaje=res.mensaje;
        this.mostrarExito=true;
        this.mostrarAlerta=false;
      },error=>{
        this.mensaje=error.error.mensaje;
        this.mostrarAlerta=true;
        this.mostrarExito=false;
      })
      
    }
  }
}
