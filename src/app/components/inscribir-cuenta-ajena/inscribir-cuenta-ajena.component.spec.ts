import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscribirCuentaAjenaComponent } from './inscribir-cuenta-ajena.component';

describe('InscribirCuentaAjenaComponent', () => {
  let component: InscribirCuentaAjenaComponent;
  let fixture: ComponentFixture<InscribirCuentaAjenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscribirCuentaAjenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscribirCuentaAjenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
