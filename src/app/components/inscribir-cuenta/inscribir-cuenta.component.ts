
import { CredencialesService } from './../../services/credenciales.service';
import { Router } from '@angular/router';
import { TransaccionService } from './../../services/transaccion.service';
import { CuentaService } from './../../services/cuenta.service';
import { Cuenta } from './../../domain/cuenta';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inscribir-cuenta',
  templateUrl: './inscribir-cuenta.component.html',
  styleUrls: ['./inscribir-cuenta.component.css']
})
export class InscribirCuentaComponent implements OnInit {

  cuentas: Cuenta[];
  cuentaSeleccionada: Cuenta = null;
  mensajeAprobacion=null;
  mensajeError =null;
  dialogAprobacion: boolean=false;
  dialogError: boolean=false;
  constructor(private cs: CuentaService, private credenciales: CredencialesService, private ts: TransaccionService, private router: Router) { }

  ngOnInit() {
    this.getCuentas();
  }
  getCuentas() {
    this.cs.getAllCuentas().subscribe(res => {
      this.cuentas = res;
    })
  }
  seleccionarCuenta(cuenta: any) {
    this.cuentaSeleccionada = cuenta;
    console.log(this.cuentaSeleccionada);

  }
  registrarCuenta() {
    if (this.cuentaSeleccionada != null) {
      this.cs.registrarCuenta(this.credenciales.getCliente().clieId, this.credenciales.getCliente().tdocIdTipoDocumento, this.credenciales.getCliente().clieId, this.cuentaSeleccionada.cuenId).subscribe(res => {
        console.log(res);
        this.dialogAprobacion=true;
        this.dialogError=false;
        this.mensajeAprobacion=res.mensaje;
      }, error => {
        this.dialogAprobacion=false;
        this.dialogError=true;
        this.mensajeError=error.error.mensaje;
      })
    }

  }

}
