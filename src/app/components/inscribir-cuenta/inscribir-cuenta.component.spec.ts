import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscribirCuentaComponent } from './inscribir-cuenta.component';

describe('InscribirCuentaComponent', () => {
  let component: InscribirCuentaComponent;
  let fixture: ComponentFixture<InscribirCuentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscribirCuentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscribirCuentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
