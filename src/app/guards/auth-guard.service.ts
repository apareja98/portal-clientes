import { CredencialesService } from './../services/credenciales.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router,Route } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private credencialesService:CredencialesService, private router:Router) { }
  canActivate(next:ActivatedRouteSnapshot,state:RouterStateSnapshot):Observable<boolean> |Promise<boolean> |boolean{
    if(this.credencialesService.getIsLogged()){
      return true;
    }else{
      this.router.navigate(['/login']);
      return false;
    }
  }
}
