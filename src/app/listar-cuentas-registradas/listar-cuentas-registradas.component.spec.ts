import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarCuentasRegistradasComponent } from './listar-cuentas-registradas.component';

describe('ListarCuentasRegistradasComponent', () => {
  let component: ListarCuentasRegistradasComponent;
  let fixture: ComponentFixture<ListarCuentasRegistradasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarCuentasRegistradasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarCuentasRegistradasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
