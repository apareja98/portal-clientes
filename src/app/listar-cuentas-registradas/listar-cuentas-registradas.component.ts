import { CredencialesService } from './../services/credenciales.service';
import { CuentaService } from './../services/cuenta.service';
import { Component, OnInit } from '@angular/core';
import { Cuenta } from '../domain/cuenta';

@Component({
  selector: 'app-listar-cuentas-registradas',
  templateUrl: './listar-cuentas-registradas.component.html',
  styleUrls: ['./listar-cuentas-registradas.component.css']
})
export class ListarCuentasRegistradasComponent implements OnInit {
  cuentas: Cuenta[];
  constructor(private cuentaService:CuentaService, private cs:CredencialesService) {
   
   }
  getCuentasRegistradas(){
    this.cuentaService.getAllCuentasRegistradas().subscribe(res=>{
      this.cuentas= res;
    })
  }
  ngOnInit() {
    this.getCuentasRegistradas();
    console.log(this.cuentas);
  }

}
