import { EditarPerfilComponent } from './components/editar-perfil/editar-perfil.component';
import { InscribirCuentaAjenaComponent } from './components/inscribir-cuenta-ajena/inscribir-cuenta-ajena.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, Router } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { TransaccionesCuentaComponent } from './components/transacciones-cuenta/transacciones-cuenta.component';
import { InscribirCuentaComponent } from './components/inscribir-cuenta/inscribir-cuenta.component';
import { ListarCuentasRegistradasComponent } from './listar-cuentas-registradas/listar-cuentas-registradas.component';
import { TrasladoComponent } from './components/traslado/traslado.component';
import { ListaTransaccionesComponent } from './components/lista-transacciones/lista-transacciones.component';
import { AuthGuardService } from './guards/auth-guard.service';

const routes: Routes=[
  {path:'', redirectTo:'/login', pathMatch:'full'},
  {path:'login', component: LoginComponent},
  {path:'home', component:HomeComponent, canActivate:[AuthGuardService]},
  {path:'detalleCuenta', component:TransaccionesCuentaComponent, canActivate:[AuthGuardService]},
  {path:'inscribirCuentaPropia', component:InscribirCuentaComponent, canActivate:[AuthGuardService]},
  {path:'inscribirCuentaAjena', component:InscribirCuentaAjenaComponent, canActivate:[AuthGuardService]},
  {path:'cuentasRegistradas', component:ListarCuentasRegistradasComponent, canActivate:[AuthGuardService]},
  {path:'realizarTraslado', component:TrasladoComponent, canActivate:[AuthGuardService]},
  {path:'perfil',component:EditarPerfilComponent, canActivate:[AuthGuardService]}
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule],
  declarations: []
})


export class AppRoutingModule { }
